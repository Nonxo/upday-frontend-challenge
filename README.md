# UPDAY Frontend Challenge

The goal of this task, is to create a small front-end system that allows our editors to log in and manage the news according to the boards.

### Overview ###

This repository contains all files and dependencies needed to contribute to the existing codebase and run the project on specific browsers such e.g Google Chrome and Firefox. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). The UPDAY Frontend Challenge contains two screens:
* Login screen
* Dashboard screen for managing news according to the boards

## Technology Stack

### Tools
The core technologies utilized in this web app are:

* Typescript - A strictly typed Javascript language is used to develop the core logic of the dashboard.
* React - The JavaScript library used to building the UI interface and manage the core architecture of the dashboard.
* Redux - This is a React dependency or library used in state management across the dashboard.
* Redux-Saga - This is a middleware used for handling asynchronous calls and side effect.
* Html & CSS - These help with structuring the user Interface.
* Bootstrap - This is a UI kit built with CSS to provide proper visuals and  better user experience.


### How do I get set up? ###

* Install yarn if not already installed. At time of writing see: [https://yarnpkg.com](https://www.npmjs.com/get-npm)
* Clone the repository to your to local with `git clone`
* From the root directory run the command `yarn install` to install all dependencies in the `package.json` file. 
* To run the project on the browser use `yarn start`
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
* You can also view the hosted version of the app on this [link](https://nonxo.github.io/weather-app/)


### `yarn test`

* From the root directory run the command `yarn test`
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
