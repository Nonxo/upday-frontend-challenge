import React from "react";
import "./App.css";
import {
  Redirect,
  Route,
  BrowserRouter as Router,
  Switch,
} from "react-router-dom";
import { isAuth } from "./redux/services/login.service";
import Dashboard from "./pages/main/dashboard";
import AuthLayout from "./layout/auth.layout";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const App: React.FunctionComponent = () => {
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <AuthGuard path="/app" component={Dashboard} />
          <Route path="/" component={AuthLayout} />
        </Switch>
      </Router>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme={"colored"}
      />
    </React.Fragment>
  );
};

const AuthGuard = ({ component, ...rest }: any) => {
  const authenticated = isAuth();
  const routeComponent = (props: any) =>
    authenticated ? React.createElement(component, props) : <Redirect to="/" />;

  return <Route {...rest} render={routeComponent} />;
};

export default App;
