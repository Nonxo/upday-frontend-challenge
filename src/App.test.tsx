import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import { MemoryRouter, Router } from "react-router-dom";
import { createStore } from "redux";
import { createMemoryHistory } from "history";
import rootReducer from "./redux/reducers";

describe("Add Redux store created from the rootReducer", () => {
  let store;
  beforeEach(() => {
    store = createStore(rootReducer);
  });

  test("renders app", () => {
    const root = document.createElement("div");
    document.body.appendChild(root);

    render(
      <MemoryRouter initialEntries={["/"]}>
        <App />
      </MemoryRouter>
    );
  });

  test("login form should be in the document", () => {
    const history = createMemoryHistory();
    render(
      <Router history={history}>
        <App />
      </Router>
    );
    expect(history.location.pathname).toBe("/");
  });

  test("validate email added to login form is valid", () => {
    const email = "name@test.com";
    const reg = RegExp(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    expect(reg.test(email)).toBe(true);
  });

  test("validate email added to login form is invalid", () => {
    const email = "name.com";
    const reg = RegExp(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    expect(reg.test(email)).not.toBe(true);
  });
});
