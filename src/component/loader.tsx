import React from "react";
import { Row } from "react-bootstrap";

const Loader: React.FunctionComponent = () => {
  return (
    <React.Fragment>
      <Row className="justify-content-center align-items-center">
        <div className="bouncing-loader">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </Row>
    </React.Fragment>
  );
};

export default Loader;
