import React from "react";
import { Badge, Card, Dropdown, Row } from "react-bootstrap";
import Empty from "./empty";
import { NewsItem } from "../redux/model/news";

const NewsList = (props: any) => {
  const { newsSelector, handleShow, statusModalShow, deleteNews } = props;
  const { drafts, archives, published } = newsSelector.news;

  // Format date to human readable
  const formatDate = (dateString: any) => {
    if (dateString) {
      const d = new Date(dateString);
      return d.toDateString();
    }
  };
  return (
    <React.Fragment>
      {drafts.length === 0 &&
      archives.length === 0 &&
      published.length === 0 ? (
        <Empty />
      ) : (
        <Row className="d-flex flex-wrap mt-4">
          {drafts &&
            drafts.length > 0 &&
            drafts.map((item: NewsItem) => (
              <Card
                key={item.id}
                style={{ width: "25rem" }}
                className="shadow my-3 mx-3 rounded"
              >
                <Dropdown className="my-3 text-end">
                  <Dropdown.Toggle
                    variant="light"
                    className="bg-white border-0 p-0"
                    id="dropdown-basic"
                  >
                    <span className="material-icons">more_horiz</span>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={() => handleShow(item, true)}>
                      Edit news details
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => statusModalShow(item)}>
                      Update news status
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => deleteNews(item)}>
                      Delete news
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Card.Img
                  variant="top"
                  src={
                    item.imageURL
                      ? item.imageURL
                      : `https://www.upday.com/wp-content/themes/upday/images/upday-logo-black.svg`
                  }
                  height={150}
                />
                <Card.Body>
                  <Card.Title className="fw-bolder">{item.title}</Card.Title>
                  <Card.Text className="fw-lighter">
                    {formatDate(item.CreatedAt)}
                  </Card.Text>
                  <Card.Text className="fw-light">{item.description}</Card.Text>
                  <Badge bg="primary">{item.status}</Badge>
                </Card.Body>
              </Card>
            ))}

          {archives &&
            archives.length > 0 &&
            archives.map((item: NewsItem) => (
              <Card
                key={item.id}
                style={{ width: "25rem" }}
                className="shadow my-3 mx-3 rounded"
              >
                <Dropdown className="my-3 text-end">
                  <Dropdown.Toggle
                    variant="light"
                    className="bg-white border-0 p-0"
                    id="dropdown-basic"
                  >
                    <span className="material-icons">more_horiz</span>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={() => handleShow(item, true)}>
                      Edit news details
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => statusModalShow(item)}>
                      Update news status
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => deleteNews(item)}>
                      Delete news
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Card.Img
                  variant="top"
                  src={
                    item.imageURL
                      ? item.imageURL
                      : `https://www.upday.com/wp-content/themes/upday/images/upday-logo-black.svg`
                  }
                  height={150}
                />
                <Card.Body>
                  <Card.Title className="fw-bolder">{item.title}</Card.Title>
                  <Card.Text className="fw-lighter">
                    {formatDate(item.CreatedAt)}
                  </Card.Text>
                  <Card.Text className="fw-light">{item.description}</Card.Text>
                  <Badge bg="secondary">{item.status}</Badge>
                </Card.Body>
              </Card>
            ))}
          {published &&
            published.length > 0 &&
            published.map((item: NewsItem) => (
              <Card
                key={item.id}
                style={{ width: "25rem" }}
                className="shadow my-3 mx-3 rounded"
              >
                <Dropdown className="my-3 text-end">
                  <Dropdown.Toggle
                    variant="light"
                    className="bg-white border-0 p-0"
                    id="dropdown-basic"
                  >
                    <span className="material-icons">more_horiz</span>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={() => handleShow(item, true)}>
                      Edit news details
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => statusModalShow(item)}>
                      Update news status
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => deleteNews(item)}>
                      Delete news
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Card.Img
                  variant="top"
                  src={
                    item.imageURL
                      ? item.imageURL
                      : `https://www.upday.com/wp-content/themes/upday/images/upday-logo-black.svg`
                  }
                  height={150}
                />
                <Card.Body>
                  <Card.Title className="fw-bolder">{item.title}</Card.Title>
                  <Card.Text className="fw-lighter">
                    {formatDate(item.CreatedAt)}
                  </Card.Text>
                  <Card.Text className="fw-light">{item.description}</Card.Text>
                  <Badge bg="success">{item.status}</Badge>
                </Card.Body>
              </Card>
            ))}
        </Row>
      )}
    </React.Fragment>
  );
};

export default NewsList;
