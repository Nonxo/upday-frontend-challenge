import React from "react";
import { Col, Row } from "react-bootstrap";
import empty from "../assets/img/empty.svg";

const Empty = () => {
  return (
    <React.Fragment>
      <Row className="justify-content-center align-items-center">
        <Col lg={3} className="mt-4 text-center">
          <img src={empty} className="img-fluid" alt="News Feed" />
        </Col>
        <h5 className="font-weight-bold my-3 text-center">
          Sorry we cannot find any news
        </h5>
      </Row>
    </React.Fragment>
  );
};

export default Empty;
