import React from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { BoardProps } from "../redux/model/board";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";
import { NewsItem } from "../redux/model/news";
import { handleRequest } from "../redux/actions/actionCreator";
import { NEWS } from "../redux/actions";
import { StorageService } from "../redux/services/storage.service";
import { LoginProps } from "../redux/model/login";

const CustomModal = ({ newsItem, onHide, mode, selectedBoard }: any) => {
  const dispatch = useDispatch();
  const storage = new StorageService();
  const currentUser: LoginProps = JSON.parse(storage.getUser() as string);
  const boardSelector = useSelector((state: any) => state.BoardReducer);
  const { title, boardId, imageURL, status, description, author } = newsItem;
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<NewsItem>({
    defaultValues: {
      author: author || currentUser.email,
      title: title || "",
      boardId: boardId || selectedBoard,
      description: description || "",
      imageURL: imageURL || "",
      status: status || "draft",
    },
  });

  // Dispatch action to create news on form submit
  const onSubmit = (inputs: NewsItem) => {
    if (mode) {
      dispatch(handleRequest(NEWS.UPDATE.PENDING, { ...newsItem, ...inputs }));
    } else {
      reset();
      dispatch(handleRequest(NEWS.CREATE.PENDING, { inputs }));
    }
    onHide();
  };

  return (
    <Form className="p-3" onSubmit={handleSubmit(onSubmit)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {mode ? "Edit News" : "Create News"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="title">
            <Form.Label>News Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter title"
              {...register("title", { required: "This is required." })}
            />
            {errors && errors.title && errors.title.type === "required" && (
              <ErrorMessage name="title" errors={errors} as="small" />
            )}
          </Form.Group>

          <Form.Group as={Col} controlId="author">
            <Form.Label>Author</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              {...register("author", {
                required: "This is required.",
                pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              })}
            />
            {errors && errors.author && errors.author.type === "required" && (
              <ErrorMessage name="author" errors={errors} as="small" />
            )}
          </Form.Group>
        </Row>

        <Form.Group className="mb-3" controlId="description">
          <Form.Label>News Description</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Add news description"
            {...register("description", { required: "This is required." })}
          />
          {errors &&
            errors.description &&
            errors.description.type === "required" && (
              <ErrorMessage name="description" errors={errors} as="small" />
            )}
        </Form.Group>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="boardId" className="mb-3">
            <Form.Label>News Board</Form.Label>
            <Form.Select
              aria-label="Board select"
              {...register("boardId", { required: "This is required." })}
              defaultValue=""
              disabled
            >
              <option disabled value="">
                Select Board
              </option>
              {boardSelector &&
                boardSelector.boards.map((obj: BoardProps) => (
                  <option key={obj.id} value={obj.id}>
                    {obj.name}
                  </option>
                ))}
            </Form.Select>
            {errors && errors.boardId && errors.boardId.type === "required" && (
              <ErrorMessage name="boardId" errors={errors} as="small" />
            )}
          </Form.Group>

          <Form.Group as={Col} controlId="status" className="mb-3">
            <Form.Label>News Status</Form.Label>
            <Form.Select
              {...register("status", { required: "This is required." })}
              disabled
            >
              <option disabled value="">
                Select Status
              </option>
              <option value="draft">Draft</option>
              <option value="published">Published</option>
              <option value="archived">Archive</option>
            </Form.Select>
            {errors && errors.status && errors.status.type === "required" && (
              <ErrorMessage name="status" errors={errors} as="small" />
            )}
          </Form.Group>
          <Form.Group className="mb-3" controlId="imageURL">
            <Form.Label>Image Url</Form.Label>
            <Form.Control
              as="textarea"
              placeholder="Add image url"
              {...register("imageURL")}
            />
          </Form.Group>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" type="submit">
          Save
        </Button>
        <Button variant="outline-secondary" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Form>
  );
};

export default CustomModal;
