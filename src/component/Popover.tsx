import { ListGroup, ListGroupItem, Popover } from "react-bootstrap";

export const popover = (
  <Popover id="menu-popover">
    <Popover.Header as="h4">Actions</Popover.Header>
    <Popover.Body>
      <ListGroup>
        <ListGroupItem>Edit news</ListGroupItem>
        <ListGroupItem>Delete news</ListGroupItem>
      </ListGroup>
    </Popover.Body>
  </Popover>
);
