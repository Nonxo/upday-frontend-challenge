import React from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";
import { NewsItem } from "../redux/model/news";
import { useDispatch } from "react-redux";
import { handleRequest } from "../redux/actions/actionCreator";
import { STATUS_UPDATE } from "../redux/actions";
import { toast } from "react-toastify";

const NewsStatusModal = ({ onHide, newsItem }: any) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      status: newsItem.status || "draft",
    },
  });

  const onSubmit = (inputs: any) => {
    const { id, boardId } = newsItem;
    const { status } = inputs;
    if (status === "published" && !newsItem.imageURL) {
      toast.error("ImageUrl field cannot be blank");
      return;
    }
    if (status === "archived") {
      dispatch(handleRequest(STATUS_UPDATE.PENDING, { status: "archive", id }));
    } else {
      dispatch(handleRequest(STATUS_UPDATE.PENDING, { status, id, boardId }));
    }
    onHide();
  };

  return (
    <Form className="p-3" onSubmit={handleSubmit(onSubmit)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Update News Status
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="status" className="mb-3">
            <Form.Label>News Status</Form.Label>
            <Form.Select
              {...register("status", { required: "This is required." })}
              defaultValue=""
            >
              <option disabled value="">
                Select Status
              </option>
              <option value="draft">Draft</option>
              <option value="published">Published</option>
              <option value="archived">Archive</option>
            </Form.Select>
            {errors && errors.status && errors.status.type === "required" && (
              <ErrorMessage name="status" errors={errors} as="small" />
            )}
          </Form.Group>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" type="submit">
          Update
        </Button>
        <Button variant="outline-secondary" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Form>
  );
};

export default NewsStatusModal;
