import React from "react";
import Logo from "../../assets/img/download.png";
import { Button, Col, Form, Image, Row, Spinner } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { LoginProps } from "../../redux/model/login";
import { ErrorMessage } from "@hookform/error-message";
import { StorageService } from "../../redux/services/storage.service";
import { useHistory } from "react-router-dom";
const Auth = () => {
  const [loading, setLoading] = React.useState(false);
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<LoginProps>();
  const storageService = new StorageService();
  const history = useHistory();

  const onSubmit = (data: LoginProps) => {
    storageService.setUser(data);
    history.push("/app");
  };

  return (
    <React.Fragment>
      <div className="my-5 px-md-5 px-3 position-absolute">
        <Image src={Logo} width={50} height={50} alt="logo" />
      </div>
      <Row className="justify-content-center align-items-center h-100">
        <Col md={7} className="text-start">
          <h5 className="fw-bold mb-2">Welcome Back!</h5>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group>
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                {...register("email", {
                  required: "This is required.",
                  pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
              />
              {errors.email && errors.email.type === "required" && (
                <ErrorMessage name="email" errors={errors} as="small" />
              )}
            </Form.Group>
            <div className="d-grid">
              <Button
                variant="primary"
                type="submit"
                className="mt-4"
                size="lg"
              >
                {loading ? (
                  <React.Fragment>
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      variant="dark"
                    />
                  </React.Fragment>
                ) : (
                  "Sign In"
                )}
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Auth;
