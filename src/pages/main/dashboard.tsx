import React, { useEffect, useState } from "react";
import { Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { handleRequest } from "../../redux/actions/actionCreator";
import { BOARD, NEWS } from "../../redux/actions";
import { BoardProps } from "../../redux/model/board";
import NewsList from "../../component/news-list";
import CustomModal from "../../component/Modal";
import Loader from "../../component/loader";
import { NewsItem } from "../../redux/model/news";
import NewsStatusModal from "../../component/status-modal";

const Dashboard: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const [selectedBoard, setSelectedBoard] = useState<string>("");
  const boardSelector = useSelector((state: any) => state.BoardReducer);
  const newsSelector = useSelector((state: any) => state.NewsReducer);
  const [modalShow, setModalShow] = useState(false);
  const [specificNews, setSpecificNews] = useState<NewsItem>({
    title: "",
    description: "",
    status: "",
    imageURL: "",
    author: "",
    boardId: "",
  });
  const [editMode, setEditMode] = useState<boolean>(false);

  const [statusModal, setStatusModal] = useState(false);

  // Dispatch an action to fetch all boards
  useEffect(() => {
    dispatch(handleRequest(BOARD.READ.PENDING, {}));
  }, []);

  // Handle board selection & dispatch action to fetch all news for a selected board
  const handleChange: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
    setSelectedBoard(e.currentTarget.value);
    dispatch(handleRequest(NEWS.READ.PENDING, { id: e.currentTarget.value }));
  };

  // Show Modal
  const handleShow = (item: NewsItem, mode: boolean) => {
    setSpecificNews(item);
    setModalShow(true);
    setEditMode(mode);
  };

  // Hide Modal
  const handleClose = () => {
    setModalShow(false);
    setEditMode(false);
    setSpecificNews({
      title: "",
      description: "",
      status: "",
      imageURL: "",
      author: "",
      boardId: "",
    });
  };

  // Show modal to update status
  const statusModalShow = (item: NewsItem) => {
    setStatusModal(true);
    setSpecificNews(item);
  };

  // Hide modal to update status
  const statusModalClose = () => {
    setStatusModal(false);
  };

  // Delete a specific news
  const deleteNews = (item: NewsItem) => {
    dispatch(
      handleRequest(NEWS.DELETE.PENDING, { id: item.id, boardId: item.boardId })
    );
  };

  return (
    <React.Fragment>
      <Container fluid>
        <Row className="justify-content-center m-3">
          <Col md={5}>
            <Form.Select
              aria-label="Board select"
              onChange={handleChange}
              defaultValue=""
            >
              <option disabled value="">
                Select Board
              </option>
              {boardSelector &&
                boardSelector.boards.map((obj: BoardProps) => (
                  <option key={obj.id} value={obj.id}>
                    {obj.name}
                  </option>
                ))}
            </Form.Select>
          </Col>
        </Row>
        <Row className="d-flex flex-wrap mt-4">
          {selectedBoard && selectedBoard !== "it" && (
            <div className="text-end">
              <Button
                variant="primary"
                size="lg"
                onClick={() => setModalShow(true)}
              >
                Create News
              </Button>
            </div>
          )}
          {newsSelector.loading ? (
            <Loader />
          ) : (
            <NewsList
              newsSelector={newsSelector}
              handleShow={handleShow}
              statusModalShow={statusModalShow}
              deleteNews={deleteNews}
            />
          )}
          <Modal
            size="lg"
            show={modalShow}
            onHide={handleClose}
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            {modalShow && (
              <CustomModal
                newsItem={specificNews}
                mode={editMode}
                onHide={handleClose}
                selectedBoard={selectedBoard}
              />
            )}
          </Modal>
          <Modal
            show={statusModal}
            onHide={statusModalClose}
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            {statusModal && (
              <NewsStatusModal
                onHide={statusModalClose}
                newsItem={specificNews}
              />
            )}
          </Modal>
        </Row>
      </Container>
    </React.Fragment>
  );
};

export default Dashboard;
