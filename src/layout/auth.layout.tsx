import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Redirect, Route, Switch } from "react-router-dom";
import Auth from "../pages/auth/auth";
import Background from "../assets/img/Inbox.jpeg";

const AuthLayout: React.FunctionComponent = (props: any) => {
  const { match } = props;
  return (
    <React.Fragment>
      <Container fluid className="vh-100 p-0">
        <Row className="h-100 m-0 p-0">
          <Col md={5}>
            <Switch>
              <Redirect exact from="/" to="/auth" />
              <Route exact path="/auth" component={Auth} />
            </Switch>
          </Col>
          <Col
            md={7}
            className="h-100 d-md-block d-none"
            style={{
              backgroundImage: `url(${Background})`,
              backgroundSize: "cover",
            }}
          />
        </Row>
      </Container>
    </React.Fragment>
  );
};

export default AuthLayout;
