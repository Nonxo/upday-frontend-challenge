import React from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Dashboard from "../pages/main/dashboard";

const PortalLayout = (props: any) => {
  const { match } = props;
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route path={`${match.url}/dashboard`} component={Dashboard} />
        </Switch>
      </Router>
    </React.Fragment>
  );
};
