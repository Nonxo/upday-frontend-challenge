import { put } from "redux-saga/effects";
import { call, takeLatest } from "redux-saga/effects";
import { BOARD } from "../actions";
import { fetchBoards } from "../services/board.service";
import { handleError, handleSuccess } from "../actions/actionCreator";

function* read() {
  try {
    const res = yield call(fetchBoards);
    if (res.status === 200) {
      yield put(handleSuccess(BOARD.READ.SUCCESS, res.data));
    } else {
      yield put(handleSuccess(BOARD.READ.ERROR, res.data));
    }
  } catch (e) {
    yield put(handleError(BOARD.READ.ERROR, e));
  }
}

export default function* watchBoard() {
  yield takeLatest(BOARD.READ.PENDING, read);
}
