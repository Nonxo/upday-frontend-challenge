import { all } from "redux-saga/effects";
import watchNews from "./news.saga";
import watchBoard from "./board.saga";

function* rootSaga() {
  yield all([watchNews(), watchBoard()]);
}

export default rootSaga;
