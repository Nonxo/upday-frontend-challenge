import { call, put, takeLatest } from "redux-saga/effects";
import { NEWS, STATUS_UPDATE } from "../actions";
import {
  createNews,
  deleteNews,
  fetchBoardNews,
  updateNews,
  updateNewsStatus,
} from "../services/news.service";
import {
  handleError,
  handleRequest,
  handleSuccess,
} from "../actions/actionCreator";
import { toast } from "react-toastify";

function* create(action: any) {
  try {
    const { inputs } = action.payload;
    const res = yield call(createNews, inputs);
    console.log(res);
    if (res.status === 200) {
      toast.success("News created");
      yield put(handleRequest(NEWS.READ.PENDING, { id: res.data.boardId }));
    } else {
      yield put(handleError(NEWS.CREATE.FAILED, res.data));
    }
  } catch (e) {
    toast.error("Something went wrong");
    yield put(handleError(NEWS.CREATE.FAILED, e));
  }
}

function* read(action: any) {
  try {
    const { id } = action.payload;
    const res = yield call(fetchBoardNews, id);
    if (res.status === 200) {
      yield put(handleSuccess(NEWS.READ.SUCCESS, res.data));
    } else {
      yield put(handleError(NEWS.READ.FAILED, res.data));
    }
  } catch (e) {
    debugger;
    toast.error("Not found");
    yield put(handleError(NEWS.READ.FAILED, e));
  }
}

function* update(action: any) {
  try {
    const res = yield call(updateNews, action.payload);
    if (res.status === 200) {
      toast.success("News updated");
      yield put(handleSuccess(NEWS.UPDATE.SUCCESS, res.data));
    } else {
      yield put(handleError(NEWS.UPDATE.FAILED, res.data));
    }
  } catch (e) {
    toast.error("Something went wrong");
    yield put(handleError(NEWS.UPDATE.FAILED, e));
  }
}

function* remove(action: any) {
  try {
    const { id, boardId } = action.payload;
    const res = yield call(deleteNews, id);
    if (res.status === 200) {
      toast.success("News deleted");
      yield put(handleRequest(NEWS.READ.PENDING, { id: boardId }));
    } else {
      yield put(handleError(NEWS.DELETE.FAILED, res.data));
    }
  } catch (e) {
    toast.error("Something went wrong");
    yield put(handleError(NEWS.DELETE.FAILED, e));
  }
}

function* updateStatus(action: any) {
  try {
    const { id, status, boardId } = action.payload;
    const res = yield call(updateNewsStatus, id, status);
    if (res.status === 200) {
      toast.success("News status updated");
      yield put(handleRequest(NEWS.READ.PENDING, { id: boardId }));
    } else {
      yield put(handleError(STATUS_UPDATE.FAILED, res.data));
    }
  } catch (e) {
    toast.error("Something went wrong");
    yield put(handleError(STATUS_UPDATE.FAILED, e));
  }
}

export default function* watchNews() {
  yield takeLatest(NEWS.CREATE.PENDING, create);
  yield takeLatest(NEWS.READ.PENDING, read);
  yield takeLatest(NEWS.UPDATE.PENDING, update);
  yield takeLatest(STATUS_UPDATE.PENDING, updateStatus);
  yield takeLatest(NEWS.DELETE.PENDING, remove);
}
