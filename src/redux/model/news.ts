export type NewsProp = {
  drafts: NewsItem[];
  published: NewsItem[];
  archives: NewsItem[];
};

export type NewsItem = {
  id?: string;
  boardId: string;
  author: string;
  title: string;
  description: string;
  imageURL: string;
  CreatedAt?: string;
  status: string;
};
