export const handleRequest = (type: string, payload: any) => ({ type, payload })
export const handleSuccess = (type: string, data: any) => ({ type, data })
export const handleError = (type: string, error: any) => ({ type, error })
