import { actionCRUD, actionType } from "./actionTypes";

export const STATUS_UPDATE = actionType("STATUS_UPDATE");
export const NEWS = actionCRUD("NEWS");
export const BOARD = actionCRUD("BOARD");
