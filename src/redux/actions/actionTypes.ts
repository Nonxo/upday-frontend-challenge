export const actionType = (type: string) => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILED: `${type}_FAILED`,
  PENDING: `${type}_PENDING`,
});

export const actionCRUD = (type: string): any => {
  let status = ["CREATE", "UPDATE", "PATCH", "READ", "DELETE"];
  let crud = {};

  status.forEach((element: string) => {
    let statusObject = {
      [element]: {
        PENDING: `${element}_${type}_PENDING`,
        SUCCESS: `${element}_${type}_SUCCESS`,
        FAILED: `${element}_${type}_FAILED`,
        CLEAR: `${element}_${type}_CLEAR`,
      },
    };
    crud = { ...crud, ...statusObject };
  });
  return crud;
};
