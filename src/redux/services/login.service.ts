import { StorageService } from "./storage.service";

export const isAuth = () => {
  let storage = new StorageService();
  return !!storage.getUser();
};
