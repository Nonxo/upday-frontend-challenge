const BASE_URL = "http://localhost:8080/v1";

export const Endpoints = {
  // News Endpoints
  NEWS: `${BASE_URL}/news`,

  // Board Endpoints
  BOARD: `${BASE_URL}/board`,
};
