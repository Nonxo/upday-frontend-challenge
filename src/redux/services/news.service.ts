import axios from "axios";
import { Endpoints } from "./enviroment";
import { NewsItem } from "../model/news";

export const createNews = (news: NewsItem) => {
  return axios.post(Endpoints.NEWS, news).then((response) => response);
};

export const fetchBoardNews = (boardId: string) => {
  return axios
    .get(Endpoints.BOARD + `/${boardId}/news`)
    .then((response) => response);
};

export const updateNews = (news: NewsItem) => {
  return axios.put(Endpoints.NEWS, news).then((response) => response);
};

export const updateNewsStatus = (newsId: string, status: string) => {
  return axios
    .post(Endpoints.NEWS + `/${newsId}/${status}`)
    .then((response) => response);
};

export const deleteNews = (newsId: string) => {
  return axios
    .delete(Endpoints.NEWS + `/${newsId}`)
    .then((response) => response);
};
