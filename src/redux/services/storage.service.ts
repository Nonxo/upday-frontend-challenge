import { LoginProps } from "../model/login";

export class StorageService {
  setUser(user: LoginProps) {
    localStorage.setItem("current_user", JSON.stringify(user));
  }

  getUser() {
    return localStorage.getItem("current_user");
  }
}
