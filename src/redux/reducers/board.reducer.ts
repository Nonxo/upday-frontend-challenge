import { BoardProps } from "../model/board";
import { BOARD } from "../actions";

const boards: BoardProps[] = [];

export const BoardReducer = (
  state = { boards, loading: true },
  action: any
) => {
  switch (action.type) {
    case BOARD.READ.PENDING:
      return {
        ...state,
        loading: true,
      };
    case BOARD.READ.SUCCESS:
      return {
        ...state,
        loading: false,
        boards: action.data,
      };
    case BOARD.READ.ERROR:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
};
