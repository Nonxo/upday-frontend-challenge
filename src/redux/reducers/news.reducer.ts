import { NEWS, STATUS_UPDATE } from "../actions";
import { NewsProp } from "../model/news";

export const NewsReducer = (
  state = { news: { drafts: [], published: [], archives: [] }, loading: false },
  action: any
) => {
  switch (action.type) {
    case NEWS.CREATE.PENDING:
      return {
        ...state,
        loading: true,
      };
    case NEWS.CREATE.SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case NEWS.CREATE.ERROR:
      return {
        ...state,
        loading: false,
      };
    case NEWS.READ.PENDING:
      return {
        ...state,
        loading: true,
      };
    case NEWS.READ.SUCCESS:
      return {
        ...state,
        loading: false,
        news: action.data,
      };
    case NEWS.READ.FAILED:
      return {
        ...state,
        loading: false,
      };
    case NEWS.UPDATE.PENDING:
      return {
        ...state,
        loading: true,
      };
    case NEWS.UPDATE.SUCCESS:
      return {
        ...state,
        loading: false,
        news: updateNews(state.news, action),
      };
    case NEWS.UPDATE.FAILED:
      return {
        ...state,
        loading: false,
      };
    case NEWS.DELETE.PENDING:
      return {
        ...state,
        loading: true,
      };
    case NEWS.DELETE.SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case NEWS.DELETE.FAILED:
      return {
        ...state,
        loading: false,
      };
    case STATUS_UPDATE.PENDING:
      return {
        ...state,
        loading: true,
      };
    case STATUS_UPDATE.SUCCESS:
      return {
        ...state,
        loading: false,
        news: updateNews(state.news, action),
      };

    case STATUS_UPDATE.FAILED:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
};

const updateNews = (state: NewsProp, action: any) => {
  if (action.data.status === "draft") {
    let newsIndex = state.drafts.findIndex((obj) => obj.id === action.data.id);
    return {
      drafts: state.drafts.map((draft, i) =>
        i === newsIndex ? { ...state.drafts[newsIndex], ...action.data } : draft
      ),
      published: state.published,
      archives: state.archives,
    };
  }

  if (action.data.status === "published") {
    let newsIndex = state.published.findIndex(
      (obj) => obj.id === action.data.id
    );
    return {
      published: state.published.map((publish, i) =>
        i === newsIndex
          ? { ...state.drafts[newsIndex], ...action.data }
          : publish
      ),
      drafts: state.drafts,
      archives: state.archives,
    };
  }

  if (action.data.status === "archive") {
    let newsIndex = state.archives.findIndex(
      (obj) => obj.id === action.data.id
    );
    return {
      archives: state.archives.map((archive, i) =>
        i === newsIndex
          ? { ...state.drafts[newsIndex], ...action.data }
          : archive
      ),
      drafts: state.drafts,
      published: state.published,
    };
  }
};
