import { combineReducers } from "redux";
import { NewsReducer } from "./news.reducer";
import { BoardReducer } from "./board.reducer";

const rootReducer = combineReducers({
  NewsReducer,
  BoardReducer,
});

export default rootReducer;
